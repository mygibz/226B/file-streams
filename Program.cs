﻿using System.IO;
using System.IO.Enumeration;
using System.Text;

namespace File_Streams
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = @"C:\Users\Yanik Acker\Desktop\hello.txt";
            string fileContent = "Foo Bar";

            WriteString(fileName, fileContent);
        }

       static  void WriteString(string fileName, string fileContent)
        {
            FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate);

            byte[] tmp = new UTF8Encoding(true).GetBytes(fileContent);
            fs.Write(tmp, 0, tmp.Length);

            fs.Close();
        }
    }
}
